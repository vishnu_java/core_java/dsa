/*
 * 6. WAP to calculate the sum of digits of a given positive integer.
 */

class code6 {
        int sumOfDigit(int num){
                if(num==0){
                        return 0;
                }
                return num%10 + sumOfDigit(num/10);

        }
        public static void main(String[] args){
                int num = 143;
                code6 obj = new code6();
                System.out.println(obj.sumOfDigit(num));
        }
}
