/*
 *      WAP to print sum of n natural numbers.
 */

class SumOfNatural{

    int sumFun(int num){

        /*for(int i=1;i<=num;i++){

            sum+=i;
        }
        System.out.println(sum);
        */

        if(num==0)
            return 0;

        return num + sumFun(--num);

 
    }

    public static void main(String[] args){

        SumOfNatural obj  = new SumOfNatural();
        System.out.println(obj.sumFun(10));
    }
}
