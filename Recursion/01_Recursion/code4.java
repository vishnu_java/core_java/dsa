/*
 * 4. WAP to print the length of digits in a number.
 */

class code4 {

        static int length(int num) {

                if (num == 0) {
                        return 0;
                }
                return 1 + length(num / 10);
        }
        public static void main(String[] args) {

                int num = 23454;
                System.out.println(length(num));
        }
}
