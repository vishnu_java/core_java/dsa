/*
 * WAP to display the first 10 natural numbers in reverse order.                           
 */  

class Reversenum{

    void rev(int num){

        if(num==0)
            return;
        System.out.println(num);
        rev(--num);
    }

    public static void main(String[] args){

        Reversenum obj = new Reversenum();
        obj.rev(10);
    }
}
