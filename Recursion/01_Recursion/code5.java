/*
 * 5. WAP to check whether the number is prime or not.
 */
class code5 {
        static boolean prime(int num,int devisor){
                
                if(num<2){
                        return false;
                }

                if(devisor == num){
                        return true;
                }

                if(num%devisor == 0){
                        return false;
                }

                return prime(num, devisor+1);
        }
        public static void main(String[]args){
                int num = 15;
                if(prime(num,2)== true){
                        System.out.println("Prime");
                }else{
                        System.out.println("Not a Prime");
                }
        }
}
