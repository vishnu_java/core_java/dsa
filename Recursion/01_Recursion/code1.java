/*
 *    WAP to print the nmbers btn 1 to 10
 */

class Numprint{

  void printNum(int num){

    if(num == 0)
      return;
    printNum(--num);
    System.out.println(++num);
  }


  public static void main(String[] args){

        Numprint obj = new Numprint();
        obj.printNum(10);
  }
}
