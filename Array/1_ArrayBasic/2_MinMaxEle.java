/*
 * Given an integer Array A of size N.
 * Find the minimum and Maximum elements in the Array.
 */

class MinMaxEle {
    
    static void minmax(int arr[]){

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for(int i=0;i<arr.length;i++){

            if(arr[i]<min)
                min=arr[i];
            
            if(arr[i]>max)
                max = arr[i];

        }
        System.out.println("Minimum: "+min);
        System.out.println("Maximum: "+max);
    }

    public static void main(String[] args) {
        int arr[] = new int[]{3,2,1,56,1000,167};
        
        minmax(arr);
    }
}