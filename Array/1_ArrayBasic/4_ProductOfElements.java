
/**
 * 4] Product of array elements
 * 
 * This is a functional problem. Your task is to return the product of array
 * elements under a given modulo.
 */

class ProductOfElements {

    static int product(int arr[]) {

        int prod = 1;

        for (int i = 0; i < arr.length; i++) {

            prod = prod * arr[i];
        }
        return prod;

    }

    public static void main(String[] args) {

        int arr[] = new int[] { 2, 3, 4, 5 };
        System.out.println(product(arr));
    }
}