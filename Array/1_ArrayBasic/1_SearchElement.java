
/**
 * 1_SearchElement 
 *  Given an integer Array and another integer element. the task is to find if the given element is present in the array or not
 * if Found return its index or not return -1.
 * 
 */
import java.util.*;
class SearchElement {

    static int search(int[] arr,int e){

        int index=-1;
        for(int i=0;i<arr.length;i++){

            if(arr[i]==e){
                index = i;
                break;
            }
        }
        return index; 
    }

    public static void main(String[] args) {
        
        @SuppressWarnings("resource")
        Scanner sc =new Scanner(System.in);
        int arr[] = new int[]{1,2,3,4,5};

        System.out.print("Enter element to search: ");
        int n = sc.nextInt();
        search(arr,n);
        System.out.println(search(arr, n));
    }
    
}