/* 
    12 First and last occurrences of X Given a sorted array having N elements, find the indices of the first and last occurrences of an element X in the given array.
    
    Note: If the number X is not found in the array, return '-1' as an array.

    Example 1:
    Input: N = 4 , X = 3
    arr[] = { 1, 3, 3, 4 }
    Output: 1 2

    Explanation: For the above array, first occurance of X = 3 is at index = 1 and last occurrence is at index = 2.

    Example 2:
    Input: N = 4, X = 5
    arr[] = { 1, 2, 3, 4 }
    Output: -1

    Explanation: As 5 is not present in the array, so the answer is -1.

    Expected Time Complexity: O(log(N))
    Expected Auxiliary Space: O(1)

    Constraints:    1 <= N <= 10^5
                    0 <= arr[i], X <= 10^9
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class FirstLastOccurrence {

    public static int[] FisrtOccurrence(int N, int X, int[] arr){

        int i=0;
        int j= N-1;
        int s = -1;
        int e = -1;

        while (i<N) {

            if(arr[i] == X){

                s = i;
                break; 
            }
            i++;
        }

        while (j>=0){
            if(arr[j] == X){
                e = j;
                break;
            }
            j--;
        }

        if(s == -1){
            return new int[]{-1};
        }else{
            return new int[]{s,e};
        }

    }

    public static void main(String[] args) throws IOException{
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Size of Array:");
        int N = Integer.parseInt(br.readLine());

        int arr[] = new int[N];

        System.out.println("Enter Elements:");
        for(int i=0;i<N;i++){
            arr[i] = Integer.parseInt(br.readLine());
        }
   
        System.out.print("Enter element to search: ");
        int X = Integer.parseInt(br.readLine());

        int occurrence [] = FisrtOccurrence(N, X, arr);

        for(int i=0;i<occurrence.length;i++){
            System.out.print(occurrence[i]+" ");
        }
        
    }
}
