/*
  13 Find unique element

    Given an array of size n which contains all elements occurring in multiples of K, except one element which doesn't occur in multiple of K. Find that unique element.

    Example 1:
    Input : n = 7, k = 3   arr[] = {6, 2, 5, 2, 2, 6, 6}
    Output : 5

    Explanation: Every element appears 3 times except 5.

    Example 2:
    Input : n = 5, k = 4    arr[] = {2, 2, 2, 10, 2}
    Output : 10

    Explanation:
        Every element appears 4 times except 10.

        Expected Time Complexity: O(N. Log(A[i]) )
        Expected Auxiliary Space: O( Log(A[i]) )
        Constraints:
                    3<= N<=2*10^5
                    2<= K<=2*10^5
                    1<= A[i]<=10^9
*/

import java.util.Arrays;

class UniqueElement {
    
    public static void uniqueEle(int N,int K,int arr[]){

        int num = 0;
        for(int i=0;i<N;i++){

            int cnt = 0;
            for(int j=i;j<N;j++){
                
                if(arr[j] == num){
                    arr[j]= 0;
                }
                if(arr[i] == arr[j])
                    cnt++;
                
            }

            num =arr[i];
            if(cnt != K && arr[i]!= 0){
                System.out.println(arr[i]);
            }
        }

    }
    public static void main(String[] args) {
        
        int arr[] = {6, 2, 5, 2, 2, 6, 6};
        int N = arr.length;
        int K = 3;
        System.out.println(Arrays.toString(arr));
        uniqueEle(N,K,arr);
        System.out.println(Arrays.toString(arr));

    }
}
