/*
 * 5] Replace all 0's with 5

    You are given an integer N. You need to convert all zeros of N to 5.
    Example 1:
    Input:  N = 1004
    Output: 1554
    Explanation: There are two zeroes in 1004 on replacing all zeroes with "5", the new number will be "1554".

    Example 2:
    Input: N = 121
    Output: 121
    Explanation: Since there are no zeroes in "121", the number remains as "121".

    Expected Time Complexity: O(K) where K is the number of digits in N
    Expected Auxiliary Space: O(1)

    Constraints:
    1 <= n <= 10000
 */

import java.util.Scanner;

class Replace0with5 {
 
    public static void main(String[] args) {
        
    
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Input: ");
        int  n = sc.nextInt();

        int temp  = n ;
        int number = 0;
        
        while(temp!=0){

            int rem = temp % 10;
            if(rem != 0 ){
                number = number*10+rem;
            }else{
                number = number*10+5;
            }
            temp = temp /10;
        }

        int replacedNumber = 0;
        
        while(number!=0){

            int rem = number%10;
            replacedNumber = replacedNumber*10+rem;
            number = number/10;
        }

        System.out.println("Output: "+replacedNumber);
    }
}