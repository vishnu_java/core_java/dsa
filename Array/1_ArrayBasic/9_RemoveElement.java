/*
    9] Remove an Element at Specific Index from an Array

    Given an array of a fixed length. The task is to remove an element at a specific index from the array.

    Examples 1:
    Input: arr[] = { 1, 2, 3, 4, 5 }, index = 2
    Output: arr[] = { 1, 2, 4, 5 }

    Examples 2:
    Input: arr[] = { 4, 5, 9, 8, 1 }, index = 3
    Output: arr[] = { 4, 5, 9, 1 }
*/

import java.util.Scanner;

class RemoveElement {
    

    static void removedIndex(int arr[], int index){

        if(index > 0 && index < arr.length){
     
            for(int i=index;i<arr.length-1;i++){
            
                arr[i]=arr[i+1];
           }

            for(int i =0;i<arr.length-1;i++){
            
                System.out.print(arr[i]+" ");
            }

        }else{
            
            System.out.println("Index Invalid");
        }
    }
    public static void main(String[] args) {
        
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Enter Array Size: ");
        int size  = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter Element:");
        for(int i = 0;i<arr.length;i++){

            arr[i] = sc.nextInt(); 
            
        }

        System.out.println("Enter Index to remove Element");
        int index = sc.nextInt();

        removedIndex(arr, index);
    }
}
